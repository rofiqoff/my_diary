package apps.mydiary.rofiqoff.com.mydiary.model;

import java.util.List;

/**
 * Created by rofiqoff on 5/28/17.
 */

public class Value {
    String value;
    String message;
    List<Result> result;

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public List<Result> getResult() {
        return result;
    }
}
