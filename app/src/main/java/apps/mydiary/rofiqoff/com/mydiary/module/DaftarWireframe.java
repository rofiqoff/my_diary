package apps.mydiary.rofiqoff.com.mydiary.module;

import android.content.Context;
import android.content.Intent;

import apps.mydiary.rofiqoff.com.mydiary.view.activity.DaftarActivity;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class DaftarWireframe {

    private DaftarWireframe() {

    }

    private static class SingleToHelper {
        private static final DaftarWireframe INSTANCE = new DaftarWireframe();
    }

    public static DaftarWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, DaftarActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
