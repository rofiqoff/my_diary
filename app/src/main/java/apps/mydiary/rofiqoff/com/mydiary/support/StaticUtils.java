package apps.mydiary.rofiqoff.com.mydiary.support;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class StaticUtils {

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static boolean checkEmptyText(EditText editText) {

        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError("Tidak boleh kosong");
        }

        return !TextUtils.isEmpty(editText.getText().toString());
    }

    public static void clearText(EditText editText) {
        if (!TextUtils.isEmpty(editText.getText().toString())) {
            editText.setText("");
            editText.setFocusableInTouchMode(true);
        }
    }

}
