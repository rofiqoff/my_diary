package apps.mydiary.rofiqoff.com.mydiary.module;

import android.content.Context;
import android.content.Intent;

import apps.mydiary.rofiqoff.com.mydiary.view.activity.ListDiaryActivity;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class ListDiaryWireframe {
    private ListDiaryWireframe() {}

    private static class SingleToHelper {
        public static final ListDiaryWireframe INSTANCE = new ListDiaryWireframe();
    }

    public static ListDiaryWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, ListDiaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
