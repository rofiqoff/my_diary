package apps.mydiary.rofiqoff.com.mydiary.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import apps.mydiary.rofiqoff.com.mydiary.R;
import apps.mydiary.rofiqoff.com.mydiary.api.RegisterAPI;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ActivityDaftarBinding;
import apps.mydiary.rofiqoff.com.mydiary.model.Value;
import apps.mydiary.rofiqoff.com.mydiary.module.LoginWireframe;
import apps.mydiary.rofiqoff.com.mydiary.support.StaticUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarActivity extends AppCompatActivity {

    ActivityDaftarBinding content;

    RegisterAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_daftar);
        content.setView(this);

        api = RegisterAPI.Factory.create();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    public void daftar() {

        content.progresBar.setVisibility(View.VISIBLE);
        content.btnDaftar.setVisibility(View.GONE);

        String sUsername = content.editTextUsernameDaftar.getText().toString().trim();
        String sPassword = content.editTextPasswordDaftar.getText().toString().trim();

        if (StaticUtils.checkEmptyText(content.editTextUsernameDaftar)
                && StaticUtils.checkEmptyText(content.editTextPasswordDaftar)) {

            cekDaftar(sUsername, sPassword);

        } else {
            content.progresBar.setVisibility(View.GONE);
            content.btnDaftar.setVisibility(View.VISIBLE);
        }

    }

    void initView() {
        content.btnDaftar.setVisibility(View.VISIBLE);
        content.progresBar.setVisibility(View.GONE);
    }

    void cekDaftar(String username, String password) {

        Call<Value> call = api.daftarUser(username, password);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String message = response.body().getMessage();
                String value = response.body().getValue();
                if (value.equals("1")){
                    StaticUtils.showMessage(getBaseContext(), message);

                    toLogin();

                } else {
                    content.editTextUsernameDaftar.setText("");
                    content.editTextUsernameDaftar.setCursorVisible(true);
                    content.editTextPasswordDaftar.setText("");

                    content.progresBar.setVisibility(View.GONE);
                    content.btnDaftar.setVisibility(View.VISIBLE);

                    StaticUtils.showMessage(getBaseContext(), message);

                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                content.progresBar.setVisibility(View.GONE);
                content.btnDaftar.setVisibility(View.VISIBLE);

                String message = "Maaf, terjadi masalah. " +
                        "\nSilahkan Periksa kembali jaringan anda!";

                StaticUtils.showMessage(getBaseContext(), message);
            }
        });
    }

    void toLogin() {
        LoginWireframe.getInstance().toView(getBaseContext());
    }

}
