package apps.mydiary.rofiqoff.com.mydiary.api;

import apps.mydiary.rofiqoff.com.mydiary.BuildConfig;
import apps.mydiary.rofiqoff.com.mydiary.model.Value;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rofiqoff on 5/28/17.
 */

public interface RegisterAPI {

    @FormUrlEncoded
    @POST("/insert-data-user.php")
    Call<Value> daftarUser(@Field("username") String username,
                       @Field("password") String password);

    @FormUrlEncoded
    @POST("/ambil-data-user.php")
    Call<Value> loginUser(@Field("username") String username,
                       @Field("password") String password);

    @FormUrlEncoded
    @POST("/ambil-data-diary.php")
    Call<Value> tampilUser(@Field("username") String username);

    @FormUrlEncoded
    @POST("/insert-data-diary.php")
    Call<Value> insertDiary(@Field("username") String username,
                            @Field("judul") String judul,
                            @Field("deskripsi") String deskripsi,
                            @Field("tanggal") String tanggal,
                            @Field("bulan") String bulan,
                            @Field("tahun") String tahun,
                            @Field("waktu") String waktu);

    @FormUrlEncoded
    @POST("/delete-data-diary.php")
    Call<Value> deleteDiary(@Field("diary_id") String diaryId,
                            @Field("username") String username,
                            @Field("judul") String judul,
                            @Field("deskripsi") String deskripsi,
                            @Field("tanggal") String tanggal,
                            @Field("bulan") String bulan,
                            @Field("tahun") String tahun,
                            @Field("waktu") String waktu);

    @FormUrlEncoded
    @POST("/update-data-diary.php")
    Call<Value> updateDiary(@Field("diary_id") String diaryId,
                            @Field("username") String username,
                            @Field("judul") String judul,
                            @Field("deskripsi") String deskripsi,
                            @Field("tanggal") String tanggal,
                            @Field("bulan") String bulan,
                            @Field("tahun") String tahun,
                            @Field("waktu") String waktu);

    class Factory {
        public static RegisterAPI create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(RegisterAPI.class);
        }
    }

}
