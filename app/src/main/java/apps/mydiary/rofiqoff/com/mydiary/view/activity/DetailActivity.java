package apps.mydiary.rofiqoff.com.mydiary.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import apps.mydiary.rofiqoff.com.mydiary.R;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ActivityDetailBinding;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        initToolbar();
        initView();
    }

    void initToolbar() {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String judul = getIntent().getStringExtra("judul");
        getSupportActionBar().setTitle(judul);

        content.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void initView() {
        String deskripsi = getIntent().getStringExtra("deskripsi");
        content.contentDetail.tvDetailDeskripsi.setText(deskripsi);
    }
}
