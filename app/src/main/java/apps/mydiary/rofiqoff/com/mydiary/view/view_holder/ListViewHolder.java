package apps.mydiary.rofiqoff.com.mydiary.view.view_holder;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Random;

import apps.mydiary.rofiqoff.com.mydiary.api.RegisterAPI;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ItemListDiaryBinding;
import apps.mydiary.rofiqoff.com.mydiary.model.Result;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class ListViewHolder extends RecyclerView.ViewHolder {

    public ItemListDiaryBinding content;
    private RegisterAPI api;
    private SharedPreferences preferences;

    private String username;

    public ListViewHolder(View itemView) {
        super(itemView);
        content = DataBindingUtil.bind(itemView);

        api = RegisterAPI.Factory.create();
    }

    public void onBind(final Context context, final Result model) {

        preferences = context.getSharedPreferences("preference", MODE_PRIVATE);

        username = preferences.getString("username", "");

        content.textViewListJudul.setText(model.getJudul());
        content.textViewListDeskripsi.setText(model.getDeskripsi());
        content.textViewListTanggal.setText(model.getTanggal());
        content.textViewListBulan.setText(model.getBulan());
        content.textViewListTahun.setText(model.getTahun());
        content.textViewListWaktu.setText(model.getWaktu());

        Random random = new Random();

        int alpha = 255;
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);

        int color = Color.argb(alpha, red, green, blue);

        content.textViewListBulan.setBackgroundColor(color);

    }
}
