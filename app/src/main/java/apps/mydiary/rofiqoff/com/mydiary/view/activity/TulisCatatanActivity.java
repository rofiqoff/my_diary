package apps.mydiary.rofiqoff.com.mydiary.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import apps.mydiary.rofiqoff.com.mydiary.R;
import apps.mydiary.rofiqoff.com.mydiary.api.RegisterAPI;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ActivityTulisCatatanBinding;
import apps.mydiary.rofiqoff.com.mydiary.model.Value;
import apps.mydiary.rofiqoff.com.mydiary.module.ListDiaryWireframe;
import apps.mydiary.rofiqoff.com.mydiary.support.StaticUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TulisCatatanActivity extends AppCompatActivity {

    ActivityTulisCatatanBinding content;

    SharedPreferences sharedPreferences;
    String id_diary, username, aksi, judul, deskripsi;

    Bundle bundle;

    RegisterAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_tulis_catatan);

        api = RegisterAPI.Factory.create();

        sharedPreferences = getSharedPreferences("preference", MODE_PRIVATE);
        username = sharedPreferences.getString("username", "");

        bundle = getIntent().getExtras();
        aksi = bundle.getString("aksi");
        judul = bundle.getString("judul");
        deskripsi = bundle.getString("deskripsi");
        id_diary = bundle.getString("id");

        initToolbar(aksi, judul, deskripsi);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tulis_catatan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        Calendar calendar = Calendar.getInstance();

        int tanggal = calendar.get(Calendar.DAY_OF_MONTH);
        int bulan = calendar.get(Calendar.MONTH);
        int tahun = calendar.get(Calendar.YEAR);

        String sTanggal = String.valueOf(tanggal);
        String sTahun = String.valueOf(tahun);

        DateFormat dateFormatTime = new SimpleDateFormat("hh:mm a");
        String time = dateFormatTime.format(new Date()).toString();

        String sBulan = "";

        switch (bulan) {
            case 0:
                sBulan = "Januari";
                break;
            case 1:
                sBulan = "Februari";
                break;
            case 2:
                sBulan = "Maret";
                break;
            case 3:
                sBulan = "April";
                break;
            case 4:
                sBulan = "Mei";
                break;
            case 5:
                sBulan = "Juni";
                break;
            case 6:
                sBulan = "Juli";
                break;
            case 7:
                sBulan = "Agustus";
                break;
            case 8:
                sBulan = "September";
                break;
            case 9:
                sBulan = "Oktober";
                break;
            case 10:
                sBulan = "November";
                break;
            case 11:
                sBulan = "Desember";
                break;
        }

        if (id == R.id.action_save) {

            if (StaticUtils.checkEmptyText(content.contentTulis.tvTulisJudul)
                    && StaticUtils.checkEmptyText(content.contentTulis.tvTulisDeskripsi)) {

                String sJudul = content.contentTulis.tvTulisJudul.getText().toString();
                String sDeskripsi = content.contentTulis.tvTulisDeskripsi.getText().toString();

                if (aksi.equals("tulis")) {

                    Call<Value> call =
                            api.insertDiary(
                                    username, sJudul, sDeskripsi, sTanggal, sBulan, sTahun, time);

                    call.enqueue(new Callback<Value>() {
                        @Override
                        public void onResponse(Call<Value> call, Response<Value> response) {
                            String value = response.body().getValue();
                            String message = response.body().getMessage();
                            if (value.equals("1")) {

                                ListDiaryWireframe.getInstance().toView(getBaseContext());
                                StaticUtils.showMessage(getBaseContext(), message);

                            } else {
                                StaticUtils.showMessage(getBaseContext(), "Ada Kesalahan");
                            }
                        }

                        @Override
                        public void onFailure(Call<Value> call, Throwable t) {
                            String message = "Maaf, terjadi masalah. " +
                                    "\nSilahkan Periksa kembali jaringan anda!";

                            StaticUtils.showMessage(getBaseContext(), message);
                        }
                    });

                } else if (aksi.equals("edit")) {

                    Call<Value> call =
                            api.updateDiary(
                                    id_diary, username, sJudul, sDeskripsi, sTanggal, sBulan, sTahun, time);

                    call.enqueue(new Callback<Value>() {
                        @Override
                        public void onResponse(Call<Value> call, Response<Value> response) {
                            String value = response.body().getValue();
                            String message = response.body().getMessage();
                            if (value.equals("1")) {
                                ListDiaryWireframe.getInstance().toView(getBaseContext());
                                StaticUtils.showMessage(getBaseContext(), message);

                            } else {

                                StaticUtils.showMessage(getBaseContext(), message);

                                StaticUtils.showMessage(getBaseContext(), "Ada Kesalahan");

                            }
                        }

                        @Override
                        public void onFailure(Call<Value> call, Throwable t) {
                            String message = "Maaf, terjadi masalah. " +
                                    "\nSilahkan Periksa kembali jaringan anda!";

                            StaticUtils.showMessage(getBaseContext(), message);
                        }
                    });

                }

            }

            return true;
        }

        return true;
    }

    void initToolbar(String aksi, String judul, String deskripsi) {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (aksi.equals("edit")) {
            content.contentTulis.tvTulisJudul.setText(judul);
            content.contentTulis.tvTulisDeskripsi.setText(deskripsi);
            getSupportActionBar().setTitle("Update Diary");
        } else {
            getSupportActionBar().setTitle("Tulis Cerita");
        }

        content.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
