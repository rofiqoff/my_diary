package apps.mydiary.rofiqoff.com.mydiary.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import apps.mydiary.rofiqoff.com.mydiary.R;
import apps.mydiary.rofiqoff.com.mydiary.api.RegisterAPI;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ActivityListDiaryBinding;
import apps.mydiary.rofiqoff.com.mydiary.model.Result;
import apps.mydiary.rofiqoff.com.mydiary.model.Value;
import apps.mydiary.rofiqoff.com.mydiary.module.DetailWireframe;
import apps.mydiary.rofiqoff.com.mydiary.module.LoginWireframe;
import apps.mydiary.rofiqoff.com.mydiary.module.TulisDiaryWireframe;
import apps.mydiary.rofiqoff.com.mydiary.support.Adapter;
import apps.mydiary.rofiqoff.com.mydiary.support.StaticUtils;
import apps.mydiary.rofiqoff.com.mydiary.view.view_holder.ListViewHolder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListDiaryActivity extends AppCompatActivity {

    private static final String TAG = ListDiaryActivity.class.getSimpleName();
    ActivityListDiaryBinding content;

    RegisterAPI api;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String id_diary, username, judul, deskripsi, tanggal, bulan, tahun, waktu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_list_diary);
        content.contentList.setView(this);
        content.setView(this);

        api = RegisterAPI.Factory.create();

        sharedPreferences = getSharedPreferences("preference", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        username = sharedPreferences.getString("username", "");

        initToolbar();
        initView();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        content.contentList.progresBar.setVisibility(View.VISIBLE);
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_diary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {

            editor.remove("username");
            editor.remove("id");
            editor.commit();
            editor.clear();

            toLogin();

            return true;

        } else if (id == R.id.action_refresh) {
            content.contentList.progresBar.setVisibility(View.VISIBLE);
            loadData();
            return true;
        }

        return true;
    }

    public void tulisCatatan() {
        String sAksi = "tulis";
        toTulisCatatan(sAksi);
    }

    void initView() {
        content.contentList.progresBar.setVisibility(View.VISIBLE);
        content.contentList.list.setVisibility(View.VISIBLE);
        content.contentList.catatanKosong.setVisibility(View.GONE);
        content.contentList.textError.setVisibility(View.GONE);
    }

    void loadData() {

        Call<Value> call = api.tampilUser(username);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                content.contentList.progresBar.setVisibility(View.GONE);

                String value = response.body().getValue();
                if (value.equals("1")) {
                    content.contentList.catatanKosong.setVisibility(View.GONE);
                    content.contentList.textError.setVisibility(View.GONE);

                    List<Result> list = response.body().getResult();
                    setList(list);

                } else {
                    content.contentList.catatanKosong.setVisibility(View.VISIBLE);
                    content.contentList.textError.setVisibility(View.GONE);
                    content.contentList.list.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                content.contentList.catatanKosong.setVisibility(View.GONE);
                content.contentList.progresBar.setVisibility(View.GONE);
                content.contentList.textError.setVisibility(View.VISIBLE);

                content.fab.setEnabled(false);

                Log.e(TAG, t.getMessage());
            }
        });
    }

    void toTulisCatatan(String aksi) {
        TulisDiaryWireframe.getInstance().toView(getBaseContext(), aksi);
    }

    void initToolbar() {
        setSupportActionBar(content.toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }

    void setList(List<Result> list) {
        Adapter<Result, ListViewHolder> adapter =
                new Adapter<Result, ListViewHolder>(
                        R.layout.item_list_diary, ListViewHolder.class, Result.class, list) {
                    @Override
                    protected void bindView(final ListViewHolder holder, final Result model, int position) {
                        holder.onBind(getBaseContext(), model);
//                        judul = holder.content.textViewListJudul.getText().toString();
//                        deskripsi = holder.content.textViewListDeskripsi.getText().toString();
//                        tanggal = holder.content.textViewListTanggal.getText().toString();
//                        bulan = holder.content.textViewListBulan.getText().toString();
//                        tahun = holder.content.textViewListTahun.getText().toString();
//                        waktu = holder.content.textViewListWaktu.getText().toString()
                        holder.content.cardViewList.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                judul = holder.content.textViewListJudul.getText().toString();
                                deskripsi = holder.content.textViewListDeskripsi.getText().toString();

                                DetailWireframe.getInstance()
                                        .toView(getBaseContext(), judul, deskripsi);
                            }
                        });

                        holder.content.cardViewList.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {

                                id_diary = model.getDiary_id();
                                judul = holder.content.textViewListJudul.getText().toString();
                                deskripsi = holder.content.textViewListDeskripsi.getText().toString();
                                tanggal = holder.content.textViewListTanggal.getText().toString();
                                bulan = holder.content.textViewListBulan.getText().toString();
                                tahun = holder.content.textViewListTahun.getText().toString();
                                waktu = holder.content.textViewListWaktu.getText().toString();

                                final AlertDialog.Builder builder =
                                        new AlertDialog
                                                .Builder(new ContextThemeWrapper(
                                                ListDiaryActivity.this, R.style.myDialog));

                                View mView = getLayoutInflater()
                                        .inflate(R.layout.item_dialog, null);
                                TextView delete = (TextView) mView.findViewById(R.id.textDelete);
                                TextView edit = (TextView) mView.findViewById(R.id.textUpdate);

                                builder.setView(mView);
                                final AlertDialog dialog = builder.create();
                                dialog.show();

                                delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        deleteList(
                                                id_diary,
                                                judul,
                                                deskripsi,
                                                tanggal,
                                                bulan,
                                                tahun,
                                                waktu,
                                                dialog);
                                    }
                                });

                                edit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        editDiary(id_diary, judul, deskripsi, dialog);
                                    }
                                });

                                return true;
                            }
                        });
                    }
                };

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        content.contentList.list.setLayoutManager(layoutManager);
        content.contentList.list.setAdapter(adapter);
        content.contentList.list.setHasFixedSize(true);
    }

    void toLogin() {
        LoginWireframe.getInstance().toView(getBaseContext());
    }

    void deleteList(String id,
                    String judul,
                    String deskripsi,
                    String tanggal,
                    String bulan,
                    String tahun,
                    String waktu,
                    final AlertDialog dialog) {

        Call<Value> call = api.deleteDiary(id, username, judul, deskripsi, tanggal, bulan, tahun, waktu);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {

                    StaticUtils.showMessage(getBaseContext(), message);
                    loadData();
                    dialog.dismiss();

                } else {
                    StaticUtils.showMessage(getBaseContext(), "Ada Kesalahan");
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {
                String message = "Maaf, terjadi masalah. " +
                        "\nSilahkan Periksa kembali jaringan anda!";

                StaticUtils.showMessage(getBaseContext(), message);
            }
        });
    }

    void editDiary(String id, String judul, String deskripsi, AlertDialog dialog) {
        String aksi = "edit";

        TulisDiaryWireframe.getInstance().toView(getBaseContext(), id, aksi, judul, deskripsi);

        dialog.dismiss();
    }
}
