package apps.mydiary.rofiqoff.com.mydiary.view.activity;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import apps.mydiary.rofiqoff.com.mydiary.R;
import apps.mydiary.rofiqoff.com.mydiary.api.RegisterAPI;
import apps.mydiary.rofiqoff.com.mydiary.databinding.ActivityLoginBinding;
import apps.mydiary.rofiqoff.com.mydiary.model.Value;
import apps.mydiary.rofiqoff.com.mydiary.module.DaftarWireframe;
import apps.mydiary.rofiqoff.com.mydiary.module.ListDiaryWireframe;
import apps.mydiary.rofiqoff.com.mydiary.support.StaticUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ActivityLoginBinding content;

    RegisterAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = DataBindingUtil.setContentView(this, R.layout.activity_login);
        content.setView(this);

        api = RegisterAPI.Factory.create();

        sharedPreferences = getSharedPreferences("preference", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (sharedPreferences.contains("username")){
            toList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    public void login(){
        content.progresBar.setVisibility(View.VISIBLE);
        content.btnLogin.setVisibility(View.GONE);

        String sUsername = content.editTextUsernameLogin.getText().toString().trim();
        String sPassword = content.editTextPasswordLogin.getText().toString().trim();

        if (StaticUtils.checkEmptyText(content.editTextUsernameLogin)
                && StaticUtils.checkEmptyText(content.editTextPasswordLogin)) {

            cekLogin(sUsername, sPassword);
        } else {
            content.btnLogin.setVisibility(View.VISIBLE);
            content.progresBar.setVisibility(View.GONE);
        }

    }

    public void daftar(){
        DaftarWireframe.getInstance().toView(getBaseContext());
    }

    void initView() {
        content.btnLogin.setVisibility(View.VISIBLE);
        content.progresBar.setVisibility(View.GONE);
    }

    void cekLogin(final String username, String password) {

        Call<Value> call = api.loginUser(username, password);
        call.enqueue(new Callback<Value>() {
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();

                if (value.equals("1")){

                    editor.remove("username");
                    editor.putString("username", username);
                    editor.commit();

                    StaticUtils.showMessage(getBaseContext(), message);
                    toList();

                } else {

                    StaticUtils.clearText(content.editTextUsernameLogin);
                    StaticUtils.clearText(content.editTextPasswordLogin);

                    content.progresBar.setVisibility(View.GONE);
                    content.btnLogin.setVisibility(View.VISIBLE);

                    StaticUtils.showMessage(getBaseContext(), message);
                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {

                content.progresBar.setVisibility(View.GONE);
                content.btnLogin.setVisibility(View.VISIBLE);

                String message = "Maaf, terjadi masalah. " +
                        "\nSilahkan Periksa kembali jaringan anda!";

                StaticUtils.showMessage(getBaseContext(), message);
            }
        });
    }

    void toList() {
        ListDiaryWireframe.getInstance().toView(getBaseContext());
    }

}
