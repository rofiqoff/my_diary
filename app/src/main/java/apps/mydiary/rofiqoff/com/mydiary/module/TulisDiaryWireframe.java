package apps.mydiary.rofiqoff.com.mydiary.module;

import android.content.Context;
import android.content.Intent;

import apps.mydiary.rofiqoff.com.mydiary.view.activity.TulisCatatanActivity;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class TulisDiaryWireframe {
    private TulisDiaryWireframe() {}

    private static class SingleToHelper {
        private static final TulisDiaryWireframe INSTANCE = new TulisDiaryWireframe();
    }

    public static TulisDiaryWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context, String aksi) {
        Intent intent = new Intent(context, TulisCatatanActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("aksi", aksi);
        context.startActivity(intent);
    }

    public void toView(Context context, String id, String aksi, String judul, String deskripsi) {
        Intent intent = new Intent(context, TulisCatatanActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id", id);
        intent.putExtra("aksi", aksi);
        intent.putExtra("judul", judul);
        intent.putExtra("deskripsi", deskripsi);
        context.startActivity(intent);
    }
}
