package apps.mydiary.rofiqoff.com.mydiary.model;

/**
 * Created by rofiqoff on 5/28/17.
 */

public class Result {

    String diary_id;
    String judul;
    String deskripsi;
    String tanggal;
    String bulan;
    String tahun;
    String waktu;

    public String getDiary_id() {
        return diary_id;
    }

    public String getJudul() {
        return judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getBulan() {
        return bulan;
    }

    public String getTahun() {
        return tahun;
    }

    public String getWaktu() {
        return waktu;
    }
}
