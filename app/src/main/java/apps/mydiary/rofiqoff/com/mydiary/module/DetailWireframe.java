package apps.mydiary.rofiqoff.com.mydiary.module;

import android.content.Context;
import android.content.Intent;

import apps.mydiary.rofiqoff.com.mydiary.view.activity.DetailActivity;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class DetailWireframe {
    private DetailWireframe() {}

    private static class SingleToHelper {
        private static final DetailWireframe INSTANCE = new DetailWireframe();
    }

    public static DetailWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context, String judul, String deskripsi) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("judul", judul);
        intent.putExtra("deskripsi", deskripsi);
        context.startActivity(intent);
    }
}
