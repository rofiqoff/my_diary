package apps.mydiary.rofiqoff.com.mydiary.module;

import android.content.Context;
import android.content.Intent;

import apps.mydiary.rofiqoff.com.mydiary.view.activity.LoginActivity;

/**
 * Created by rofiqoff on 12/3/17.
 */

public class LoginWireframe {
    private LoginWireframe() {}

    private static class SingleToHelper {
        private static final LoginWireframe INSTANCE = new LoginWireframe();
    }

    public static LoginWireframe getInstance() {
        return SingleToHelper.INSTANCE;
    }

    public void toView(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
}
